# Project Description
classicmodels in this version is a demonstration of JPA against a MariaDB using Handlebars templates to render HTML.

## TL;DR;
* ..coming soon.

## Getting Started
* You will need a MySQL or MariaDB installed and running. Keep on hand the username and password to access that system.
* Using MySQL workbench or some other MySQL client, import the database using the database script in docs/sampledatabase.sql
* Adjust the username and password for the database in src/main/resources/application.yml
* mvn spring-boot:run

At this point, you hopefully can hit http://localhost:8088/ and see a page generated from Handlebars. Following the link on that page will show you customers if everything is working.

## Requirements
* JRE 8
* Tomcat 8.0.21
* MariaDB 5.5+
* Maven 3.X

## Composition
* [Spring Boot](http://projects.spring.io/spring-boot/)

## To Do
- Docker Compose -- not fully done
- Docker shell scripts -- not fully done

## References
The database was acquired from a google search of "sample database". It's origins are http://www.mysqltutorial.org/mysql-sample-database.aspx and/or http://www.richardtwatson.com/dm6e/Reader/ClassicModels.html
