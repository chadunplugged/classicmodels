## Create our own localized network for this..
docker network create classicmodelsnetwork
## Pull the mysql image and create the mysql container
cd mysql
sh runmysql.sh
cd ..
## Going to build the java app image, and then run it
cd classicmodelsJavaApp
sh build.sh
sh run.sh
## should be done!
