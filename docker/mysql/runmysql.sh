#!/usr/bin/env bash
# the -v option here specifies where the container can mount it's directory that contains the start database script.
# you may need to adjust this for your filesystem
# also note the password here (test) needs to match application.properties
docker run --name mysqlcontainer -e USERID=$UID --network classicmodelsnet -e MYSQL_ROOT_PASSWORD=test --restart unless-stopped -p 3306:3306 -v "/home/$USER/code/classicmodels/docker/mysql/dbinit:/docker-entrypoint-initdb.d" -d docker.io/mysql:5.7