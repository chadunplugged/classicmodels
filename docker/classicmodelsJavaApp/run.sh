#!/usr/bin/env bash
docker run -dt --name classicmodels --network classicmodelsnet -e USERID=$UID -p 8088:8088 --restart unless-stopped -v "/home/$USER/code/classicmodels/:/opt/classicmodels:Z" -v "/home/$USER/docker/m2:/root/.m2:Z"  -v "/home/$USER/code/classicmodels/log:/var/log/classicmodels:Z" lab131/classicmodels
