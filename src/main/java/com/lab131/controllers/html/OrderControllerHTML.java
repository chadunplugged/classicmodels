package com.lab131.controllers.html;

import com.lab131.entities.Order;
import com.lab131.repositories.OrderRepository;
import com.lab131.resources.OrderResource;
import com.lab131.service.UrlHelperSingleton;
import com.lab131.service.assemblers.OrderResourceAssembler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by chadj on 12/22/15.
 */
@RestController
@ExposesResourceFor(OrderResource.class)
public class OrderControllerHTML {

	private static Logger LOG = LoggerFactory.getLogger(OrderControllerHTML.class);

	private OrderRepository orderRepository;
	private OrderResourceAssembler orderResourceAssembler;

	@Autowired
	public OrderControllerHTML(final OrderRepository orderRepository, final OrderResourceAssembler orderResourceAssembler) {
		this.orderRepository = orderRepository;
		this.orderResourceAssembler = orderResourceAssembler;
	}

    @RequestMapping(value = "/orders/customerNumber/{customerNumber}", produces = "text/html")
    @ResponseBody
    public String findAllOrderNumbersByCustomerNumberHTML(
            @PathVariable(value = "customerNumber") int customerNumber) {

        List<Order> customerOrders = orderRepository.findAllOrderNumbersByCustomerNumber(customerNumber);
        String orders = "<table><tr>" +
				"<th>Order Number</th>" +
				"<th>Status</th>" +
				"<th>Comments</th>" +
				"<th>Order Date</th>" +
				"<th>Shipped</th>" +
				"<th>Required</th>" +
				"</tr>";
        for (Order co : customerOrders) {
			orders+="<tr>";
            orders += "<td><a href=\""+ UrlHelperSingleton.baseurl+"orderdetails/" + co.getOrderNumber()+"\">"+
					co.getOrderNumber()+"</a></td>";
			orders+="<td>"+co.getStatus()+"</td>";
			orders+="<td>"+co.getComments()+"</td>";
			orders+="<td>"+co.getCustomerNumber()+"</td>";
			orders+="<td>"+co.getOrderDate()+"</td>";
			orders+="<td>"+co.getShippedDate()+"</td>";
			orders+="<td>"+co.getRequiredDate()+"</td>";
			orders+="</tr>";
        }
		orders+="</table>";
		orders+="<br>Number of orders: "+customerOrders.size();

        return orders;
    }
}
