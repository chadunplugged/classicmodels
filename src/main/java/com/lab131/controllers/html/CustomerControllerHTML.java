package com.lab131.controllers.html;

import com.lab131.entities.Customer;
import com.lab131.repositories.CustomerRepository;
import com.lab131.repositories.UpdateHelper;
import com.lab131.resources.CustomerResource;
import com.lab131.service.UrlHelperSingleton;
import com.lab131.service.assemblers.CustomerResourceAssembler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by chadj on 12/22/15.
 */
@Controller
public class CustomerControllerHTML {

	private static Logger LOG = LoggerFactory.getLogger(CustomerControllerHTML.class);

	private final CustomerRepository customerRepository;
	private final CustomerResourceAssembler customerResourceAssembler;

	@Autowired
	public CustomerControllerHTML(final CustomerRepository customerRepository, final CustomerResourceAssembler customerResourceAssembler) {
		this.customerRepository = customerRepository;
		this.customerResourceAssembler = customerResourceAssembler;
	}

	@RequestMapping(value = "customers/{customerNumber}", produces = "text/html")
	public String findOneCustomer(
			@PathVariable(value = "customerNumber") final int customerNumber,
			Model model) {

		final Customer customer = customerRepository.findOne(customerNumber);
		model.addAttribute(customer);
		return "customerDetails";
	}

	@RequestMapping(value="/customers", produces = "text/html", method = RequestMethod.GET)
	public String findAllCustomersHTML(
			Model model, Pageable pageable) {

		Page<Customer> customersPage = customerRepository.findAllCustomersByCustomerNumberNotNull(pageable);
		List<Customer> customerList = customersPage.getContent();


		model.addAttribute(customerList);
		model.addAttribute("baseUrl", UrlHelperSingleton.baseurl);
		model.addAttribute("currentPage",pageable.getPageNumber());
		model.addAttribute("nextPage",UrlHelperSingleton.baseurl+"/customers/?page="+(pageable.getPageNumber()+1));
		model.addAttribute("prevPage",UrlHelperSingleton.baseurl+"/customers/?page="+(pageable.getPageNumber()-1));
		return "customersList";
	}
	
	@RequestMapping(value = "/updateCustomer", produces = "text/html", method = RequestMethod.POST)
	public String updateCustomer(@ModelAttribute Customer customer,
			Model model, Pageable pageable) {
		
		final Customer customerFromDB = customerRepository.findOne(customer.getCustomerNumber());
		
		//This isn't optimal probably, but it works.
		UpdateHelper.copyNonNullProperties(customer, customerFromDB);
		customerRepository.save(customerFromDB);
		
		Page<Customer> customersPage = customerRepository.findAllCustomersByCustomerNumberNotNull(pageable);
		List<Customer> customerList = customersPage.getContent();
		
		
		model.addAttribute(customerList);
		model.addAttribute("baseUrl", UrlHelperSingleton.baseurl);
		model.addAttribute("currentPage",pageable.getPageNumber());
		model.addAttribute("nextPage",UrlHelperSingleton.baseurl+"/customers/?page="+(pageable.getPageNumber()+1));
		model.addAttribute("prevPage",UrlHelperSingleton.baseurl+"/customers/?page="+(pageable.getPageNumber()-1));
		
		System.out.println("Hello "+customer.getCustomerName()+"!!!");
		return "customersList";
	}
}

