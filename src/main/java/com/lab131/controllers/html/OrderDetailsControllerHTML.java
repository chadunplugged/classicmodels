package com.lab131.controllers.html;

import com.lab131.entities.OrderDetail;
import com.lab131.repositories.OrderDetailRepository;
import com.lab131.resources.OrderDetailResource;
import com.lab131.service.UrlHelperSingleton;
import com.lab131.service.assemblers.OrderDetailResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;

/**
 * Created by chadj on 12/23/15.
 */
@RestController
@ExposesResourceFor(OrderDetailResource.class)
public class OrderDetailsControllerHTML {

    private OrderDetailRepository orderDetailRepository;
	private OrderDetailResourceAssembler orderDetailResourceAssembler;

	@Autowired
	public OrderDetailsControllerHTML(final OrderDetailRepository orderDetailRepository,
									  final OrderDetailResourceAssembler orderDetailResourceAssembler) {
		this.orderDetailRepository = orderDetailRepository;
		this.orderDetailResourceAssembler = orderDetailResourceAssembler;
	}

	@RequestMapping(value = "/orderdetails/{orderNumber}", produces = "text/html")
    public String findAllOrderNumbersByCustomerNumberHTML(
			@PathVariable(value = "orderNumber") int orderNumber) {
        List<OrderDetail> orderDetailList = orderDetailRepository.findByOrderNumber(orderNumber);
//        PagedResources pagedUserResource = assembler.toResource(orderDetails);
		StringBuilder sb = new StringBuilder();
		sb.append("<table>");
		for(OrderDetail od : orderDetailList) {
			sb.append("<tr><td>");
			sb.append(od.getOrderNumber());
			sb.append("</td><td>");
			sb.append(od.getOrderLineNumber());
			sb.append("</td><td>");
			sb.append("<a href=\""+ UrlHelperSingleton.baseurl+"/products/");
			sb.append(od.getProductCode());
			sb.append("\">");
			sb.append(od.getProductCode());
			sb.append("</a>");
			sb.append("</td><td> $");
			sb.append(od.getPriceEach());
			sb.append("</td><td>");
			sb.append(od.getQuantityOrdered());
			sb.append("</td></tr>");

		}
		sb.append("</table><br>Number of Order Detail Elements: "+ orderDetailList.size());
        return sb.toString();
    }
}
