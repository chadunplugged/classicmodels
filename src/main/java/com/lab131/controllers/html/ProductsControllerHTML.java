package com.lab131.controllers.html;

import com.lab131.entities.Product;
import com.lab131.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by chadj on 3/3/16.
 */
@Controller
public class ProductsControllerHTML {

	@Autowired
	private ProductRepository productRepository;

	@RequestMapping(value = "/products/{productCode}", produces = "text/html")
	@ResponseBody
	public String findProductFromProductCode(
			@PathVariable(value = "productCode") String productCode) {
		Product product = productRepository.findProductByProductCode(productCode);
		StringBuilder sb = new StringBuilder(100);
		sb.append(product.getProductName());
		sb.append("<br>");
		sb.append(product.getProductCode());
		sb.append("<br>Buy Price: ");
		sb.append(product.getBuyPrice());
		sb.append("<br>MSRP: ");
		sb.append(product.getMSRP());
		sb.append("<br>");
		sb.append(product.getProductDescription());
		sb.append("<br>");
		sb.append(product.getProductLine());
		sb.append("<br>");
		sb.append(product.getProductVendor());
		sb.append("<br>Quantity in stock: ");
		sb.append(product.getQuantityInStock());
		return sb.toString();
	}

}
