package com.lab131.controllers;

import com.lab131.service.UrlHelperSingleton;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by chadj on 12/16/15.
 */
@Controller
public class RootHTMLController {
	

	@RequestMapping(value = "/", produces = "text/html")
	public String htmlindex(Model model, HttpServletRequest request) {
		UrlHelperSingleton.baseurl = String.format("%s://%s:%d/",request.getScheme(),  request.getServerName(), request.getServerPort());
		model.addAttribute("baseurl", UrlHelperSingleton.baseurl);
		return "index";
	}

}
