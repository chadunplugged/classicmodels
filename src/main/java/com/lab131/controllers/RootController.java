package com.lab131.controllers;

//import com.Order;
import com.lab131.controllers.hal_json.CustomerController;
import com.lab131.service.UrlHelperSingleton;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller
public class RootController {

//    private static final String TEMPLATE = "Hello, %s!";

    @RequestMapping(value="/", produces = "application/hal+json")
    @ResponseBody
    public HttpEntity<ResourceSupport> root(HttpServletRequest request) {
	    UrlHelperSingleton.baseurl = String.format("%s://%s:%d/",request.getScheme(),  request.getServerName(), request.getServerPort());
		ResourceSupport rootResource = new ResourceSupport();

		rootResource.add(linkTo(methodOn(RootController.class).root(request)).withSelfRel());
		rootResource.add(linkTo(methodOn(CustomerController.class).findAllCustomers()).withRel("customers"));
//		rootResource.add(linkTo(methodOn(OrderController.class).getOrder(666)).withRel("order"));
//        rootResource.add(linkTo(methodOn(OrderController.class).findAllOrderNumbersByCustomerNumber(-66, null, null)).withRel("findAllOrderNumbersByCustomerNumber"));
//        rootResource.add(linkTo(methodOn(OrderController.class).getOrder(-66,null,null)).slash(null).withRel("getOrder"));
//		rootResource.add(new Link(UrlHelperSingleton.baseurl+"getOrder/{?orderNumber}").withRel("getOrderInformation"));
//		rootResource.add(new Link(UrlHelperSingleton.baseurl+"findOrderDetailsByOrderNumber/{?orderNumber}").withRel("findOrderDetailsByOrderNumber"));

		/** The following line works but not templated. But following the details of others that have made complex classes to manage
		 * templated hal links and such, Im just going to use simple strings all bundled up in one static class.
		 * especially since this is a just a sample app. it shouldn't be too terrible to manage until it hits over
		 * 50 end points.
 		 */
		//rootResource.add(linkTo(methodOn(OrderDetailsController.class).findAllOrderNumbersByCustomerNumber(0,null,null)).withRel("findOrderDetailsByOrderNumber"));
//		rootResource.add(new Link(UrlHelperSingleton.baseurl+"findAllOrderNumbersByCustomerNumber/{?customerNumber}").withRel("findAllOrderNumbersByCustomerNumber"));
//	    rootResource.add(new Link(UrlHelperSingleton.baseurl+"setdomain").withRel("setdomain"));
		rootResource.add(new Link(UrlHelperSingleton.baseurl+"hal-browser/browser.html").withRel("hal-browser"));

        return new ResponseEntity<>(rootResource, HttpStatus.OK);
    }
}
