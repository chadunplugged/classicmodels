package com.lab131.controllers;

import com.lab131.service.UrlHelperSingleton;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by chadj on 3/7/16.
 */
@Controller
public class DomainSetController {

    @RequestMapping(value = "/setdomain")
    @ResponseBody
    public String htmlindex(HttpServletRequest request) {
        UrlHelperSingleton.baseurl = String.format("%s://%s:%d",request.getScheme(),  request.getServerName(), request.getServerPort());
        return "Domain has been set to: "+UrlHelperSingleton.baseurl;
    }
}
