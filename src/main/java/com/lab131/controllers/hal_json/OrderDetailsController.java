package com.lab131.controllers.hal_json;

import com.lab131.entities.OrderDetail;
import com.lab131.repositories.OrderDetailRepository;
import com.lab131.resources.OrderDetailResource;
import com.lab131.service.UrlHelperSingleton;
import com.lab131.service.assemblers.OrderDetailResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by chadj on 12/23/15.
 */
@RestController
@ExposesResourceFor(OrderDetailResource.class)
public class OrderDetailsController {

    private OrderDetailRepository orderDetailRepository;
	private OrderDetailResourceAssembler orderDetailResourceAssembler;

	@Autowired
	public OrderDetailsController(final OrderDetailRepository orderDetailRepository,
								  final OrderDetailResourceAssembler orderDetailResourceAssembler) {
		this.orderDetailRepository = orderDetailRepository;
		this.orderDetailResourceAssembler = orderDetailResourceAssembler;
	}

	@RequestMapping(value = "/orderdetails/{orderNumber}", produces = "application/hal+json")
	public ResponseEntity<Resources<OrderDetailResource>> getOrderDetails(
			@PathVariable(value = "orderNumber") int orderNumber) {
		final Iterable<OrderDetail> orderDetails = orderDetailRepository.findByOrderNumber(orderNumber);
		final Resources<OrderDetailResource> wrapped = orderDetailResourceAssembler.toEmbeddedList(orderDetails);
		return ResponseEntity.ok(wrapped);
	}
}
