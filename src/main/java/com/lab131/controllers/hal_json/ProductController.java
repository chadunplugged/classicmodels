package com.lab131.controllers.hal_json;

import com.lab131.entities.Product;
import com.lab131.repositories.ProductRepository;
import com.lab131.resources.ProductResource;
import com.lab131.service.assemblers.ProductResourceAssembler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by chadj on 12/22/15.
 */
@RestController
@ExposesResourceFor(ProductResource.class)
public class ProductController {

	private static Logger LOG = LoggerFactory.getLogger(ProductController.class);

	private ProductRepository productRepository;
	private ProductResourceAssembler productResourceAssembler;

	@Autowired
	public ProductController(final ProductRepository pr, final ProductResourceAssembler ora) {
		this.productRepository = pr;
		this.productResourceAssembler = ora;
	}

	@RequestMapping(value = "/products/{productCode}", produces = "application/hal+json")
    public ResponseEntity<ProductResource> getProduct(
            @PathVariable(value = "productCode") String productCode) {
		final Product product = productRepository.findProductByProductCode(productCode);
		LOG.info("Not sure if this will actually work: "+productCode+" - "+product.getProductName());
		final ProductResource productResource = productResourceAssembler.toResource(product);
		return ResponseEntity.ok(productResource);
    }
}
