package com.lab131.controllers.hal_json;

import com.lab131.entities.Order;
import com.lab131.repositories.OrderRepository;
import com.lab131.resources.OrderResource;
import com.lab131.service.assemblers.OrderResourceAssembler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by chadj on 12/22/15.
 */
@RestController
@ExposesResourceFor(OrderResource.class)
public class OrderController {

	private static Logger LOG = LoggerFactory.getLogger(OrderController.class);

	private OrderRepository orderRepository;
	private OrderResourceAssembler orderResourceAssembler;

	@Autowired
	public OrderController(final OrderRepository orderRepository, final OrderResourceAssembler orderResourceAssembler) {
		this.orderRepository = orderRepository;
		this.orderResourceAssembler = orderResourceAssembler;
	}

	@RequestMapping(value = "/orders/{orderNumber}", produces = "application/hal+json")
    public ResponseEntity<OrderResource> getOrder(
            @PathVariable(value = "orderNumber") int orderNumber) {
		final Order order = orderRepository.findOne(orderNumber);
		LOG.info("Not sure if this will actually work: "+orderNumber+" - "+order.getCustomerNumber());
		final OrderResource orderResource = orderResourceAssembler.toResource(order);
		return ResponseEntity.ok(orderResource);
    }

	@RequestMapping(value = "/orders/customerNumber/{customerNumber}", produces = "application/hal+json")
	@ResponseBody
    public ResponseEntity<Resources<OrderResource>> findAllOrderNumbersByCustomerNumber(
			@PathVariable(value = "customerNumber") int customerNumber) {
        final Iterable<Order> orders = orderRepository.findAllOrderNumbersByCustomerNumber(customerNumber);
		final Resources<OrderResource> wrapped = orderResourceAssembler.toEmbeddedList(orders);
		return ResponseEntity.ok(wrapped);
    }
}
