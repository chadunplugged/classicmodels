package com.lab131.controllers.hal_json;

import com.lab131.entities.Customer;
import com.lab131.repositories.CustomerRepository;
import com.lab131.repositories.OrderRepository;
import com.lab131.resources.CustomerResource;
import com.lab131.service.assemblers.CustomerResourceAssembler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by chadj on 12/22/15.
 */
@RestController
@ExposesResourceFor(CustomerResource.class)
@RequestMapping(value="/customers", produces = "application/hal+json")
public class CustomerController {

	private static Logger LOG = LoggerFactory.getLogger(CustomerController.class);

	@Autowired
	private final CustomerRepository customerRepository;
	private final CustomerResourceAssembler customerResourceAssembler;

	@Autowired
	public CustomerController(final CustomerRepository customerRepository, final CustomerResourceAssembler customerResourceAssembler) {
		this.customerRepository = customerRepository;
		this.customerResourceAssembler = customerResourceAssembler;
	}


    @RequestMapping(method=RequestMethod.GET)
    public ResponseEntity<Resources<CustomerResource>> findAllCustomers() {
		final Iterable<Customer> customers = customerRepository.findAll();
		final Resources<CustomerResource> wrapped = customerResourceAssembler.toEmbeddedList(customers);
		LOG.debug("List all customers");
		return ResponseEntity.ok(wrapped);
    }

	@RequestMapping(value = "/{customerNumber}", produces = "application/hal+json")
	@ResponseBody
	public ResponseEntity<CustomerResource> findOneCustomer(
			@PathVariable(value = "customerNumber") final int customerNumber) {

		final Customer customer = customerRepository.findOne(customerNumber);
		final CustomerResource customerResource = customerResourceAssembler.toResource(customer);
		return ResponseEntity.ok(customerResource);
	}

	@RequestMapping(method=RequestMethod.POST, produces = "application/hal+json")
	public ResponseEntity<Void> newCustomer(@RequestBody Customer newCustomer) {
		// TODO Add input validation
		final HttpHeaders headers = new HttpHeaders();
		Customer savedCustomer = Customer.createNewCustomer(newCustomer);
		customerRepository.save(savedCustomer);
		customerRepository.flush();
		return new ResponseEntity("{\"id\":"+savedCustomer.getCustomerNumber()+"}", headers, HttpStatus.CREATED);
//		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

}

