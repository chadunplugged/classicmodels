package com.lab131.resources;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.lab131.support.ResourceWithEmbeddeds;
import org.springframework.hateoas.core.Relation;

/**
 * Created by chadj on 4/2/16.
 */
@Relation(value="customer", collectionRelation="customers")
public class CustomerResource extends ResourceWithEmbeddeds {

    private final Integer customerNumber;
    private final String customerName;
    private final String contactLastName;
    private final String contactFirstName;
    private final String phone;
    private final String addressLine1;
    private final String addressLine2;
    private final String city;
    private final String state;
    private final String postalCode;
    private final String country;
    private final String salesRepEmployeeNumber;
    private final String creditLimit;

    @JsonCreator
    public CustomerResource(
            @JsonProperty("customerNumber") Integer customerNumber,
            @JsonProperty("customerName") String customerName,
            @JsonProperty("contactLastName") String contactLastName,
            @JsonProperty("contactFirstName") String contactFirstName,
            @JsonProperty("phone") String phone,
            @JsonProperty("addressLine1") String addressLine1,
            @JsonProperty("addressLine2") String addressLine2,
            @JsonProperty("city") String city,
            @JsonProperty("state") String state,
            @JsonProperty("postalCode") String postalCode,
            @JsonProperty("country") String country,
            @JsonProperty("salesRepEmployeeNumber") String salesRepEmployeeNumber,
            @JsonProperty("creditLimit") String creditLimit) {
        this.customerNumber = customerNumber;
        this.customerName = customerName;
        this.contactLastName = contactLastName;
        this.contactFirstName = contactFirstName;
        this.phone = phone;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.country = country;
        this.salesRepEmployeeNumber = salesRepEmployeeNumber;
        this.creditLimit = creditLimit;
    }

    public Integer getCustomerNumber() {
        return customerNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getContactLastName() {
        return contactLastName;
    }

    public String getContactFirstName() {
        return contactFirstName;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCountry() {
        return country;
    }

    public String getSalesRepEmployeeNumber() {
        return salesRepEmployeeNumber;
    }

    public String getCreditLimit() {
        return creditLimit;
    }
}
