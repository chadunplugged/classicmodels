package com.lab131.resources;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.lab131.support.ResourceWithEmbeddeds;
import org.springframework.hateoas.core.Relation;

/**
 * Created by chadj (dontpanic@bodybuilding.com) on 4/19/16
 *
 * @author chadj
 * @version 1.0.0
 * @since 4/19/16
 */
@Relation(value = "product", collectionRelation = "products")
public class ProductResource extends ResourceWithEmbeddeds {
	private String productCode, productName, productLine, productScale, productVendor, productDescription, quantityInStock, buyPrice, MSRP;

	public ProductResource(
			@JsonProperty("productCode") String productCode,
			@JsonProperty("productName") String productName,
			@JsonProperty("productLine") String productLine,
			@JsonProperty("productScale") String productScale,
			@JsonProperty("productVendor") String productVendor,
			@JsonProperty("productDescription") String productDescription,
			@JsonProperty("quantityInStock") String quantityInStock,
			@JsonProperty("buyPrice") String buyPrice,
			@JsonProperty("MSRP") String MSRP) {
		this.productCode = productCode;
		this.productName = productName;
		this.productLine = productLine;
		this.productScale = productScale;
		this.productVendor = productVendor;
		this.productDescription = productDescription;
		this.quantityInStock = quantityInStock;
		this.buyPrice = buyPrice;
		this.MSRP = MSRP;
	}

	public String getProductCode() {
		return productCode;
	}

	public String getProductName() {
		return productName;
	}

	public String getProductLine() {
		return productLine;
	}

	public String getProductScale() {
		return productScale;
	}

	public String getProductVendor() {
		return productVendor;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public String getQuantityInStock() {
		return quantityInStock;
	}

	public String getBuyPrice() {
		return buyPrice;
	}

	public String getMSRP() {
		return MSRP;
	}
}
