package com.lab131.resources;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.lab131.support.ResourceWithEmbeddeds;
import org.springframework.hateoas.core.Relation;

/**
 * Created by chadj (dontpanic@bodybuilding.com) on 4/17/16
 *
 * @author chadj
 * @version 1.0.0
 * @since 4/17/16
 */
@Relation(value = "orderDetail", collectionRelation = "orderDetails")
public class OrderDetailResource extends ResourceWithEmbeddeds {

	private final int orderNumber;
	private final String productCode;
	private final String quantityOrdered;
	private final String priceEach;
	private final String orderLineNumber;

	@JsonCreator
	public OrderDetailResource(
			@JsonProperty("orderNumber") int orderNumber,
			@JsonProperty("productCode") String productCode,
			@JsonProperty("quantityOrdered") String quantityOrdered,
			@JsonProperty("priceEach") String priceEach,
			@JsonProperty("orderLineNumber") String orderLineNumber
	) {
		this.orderNumber = orderNumber;
		this.productCode = productCode;
		this.quantityOrdered = quantityOrdered;
		this.priceEach = priceEach;
		this.orderLineNumber = orderLineNumber;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public String getProductCode() {
		return productCode;
	}

	public String getQuantityOrdered() {
		return quantityOrdered;
	}

	public String getPriceEach() {
		return priceEach;
	}

	public String getOrderLineNumber() {
		return orderLineNumber;
	}
}
