package com.lab131.resources;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.lab131.support.ResourceWithEmbeddeds;
import org.springframework.hateoas.core.Relation;

/**
 * Created by chadj (dontpanic@bodybuilding.com) on 4/17/16
 *
 * @author chadj
 * @version 1.0.0
 * @since 4/17/16
 */
@Relation(value="order", collectionRelation="orders")
public class OrderResource extends ResourceWithEmbeddeds {

	private final int orderNumber;
	private final String orderDate;
	private final String requiredDate;
	private final String shippedDate;
	private final String status;
	private final String comments;
	private final int customerNumber;

	@JsonCreator
	public OrderResource(
			@JsonProperty("orderNumber") int orderNumber,
			@JsonProperty("orderDate") String orderDate,
			@JsonProperty("requiredDate") String requiredDate,
			@JsonProperty("shippedDate") String shippedDate,
			@JsonProperty("status") String status,
			@JsonProperty("comments") String comments,
			@JsonProperty("customerNumber") int customerNumber) {
		this.orderNumber = orderNumber;
		this.orderDate = orderDate;
		this.requiredDate = requiredDate;
		this.shippedDate = shippedDate;
		this.status = status;
		this.comments = comments;
		this.customerNumber = customerNumber;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public String getRequiredDate() {
		return requiredDate;
	}

	public String getShippedDate() {
		return shippedDate;
	}

	public String getStatus() {
		return status;
	}

	public String getComments() {
		return comments;
	}

	public int getCustomerNumber() {
		return customerNumber;
	}
}
