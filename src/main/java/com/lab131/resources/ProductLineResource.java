package com.lab131.resources;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.lab131.support.ResourceWithEmbeddeds;
import org.springframework.hateoas.core.Relation;

/**
 * Created by chadj (dontpanic@bodybuilding.com) on 4/19/16
 *
 * @author chadj
 * @version 1.0.0
 * @since 4/19/16
 */
@Relation(value="order", collectionRelation="orders")
public class ProductLineResource extends ResourceWithEmbeddeds {
	private String productLine, textDescription, htmlDescription, image;

	public ProductLineResource(
			@JsonProperty("productLine") String productLine,
			@JsonProperty("textDescription") String textDescription,
			@JsonProperty("htmlDescription") String htmlDescription,
			@JsonProperty("image") String image) {
		this.productLine = productLine;
		this.textDescription = textDescription;
		this.htmlDescription = htmlDescription;
		this.image = image;
	}

	public String getProductLine() {
		return productLine;
	}

	public String getTextDescription() {
		return textDescription;
	}

	public String getHtmlDescription() {
		return htmlDescription;
	}

	public String getImage() {
		return image;
	}
}
