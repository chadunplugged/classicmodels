package com.lab131.service.assemblers;

import com.lab131.controllers.hal_json.OrderController;
import com.lab131.controllers.hal_json.OrderDetailsController;
import com.lab131.entities.OrderDetail;
import com.lab131.resources.OrderDetailResource;
import com.lab131.support.EmbeddableResourceAssemblerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RelProvider;
import org.springframework.stereotype.Service;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by chadj (dontpanic@bodybuilding.com) on 4/19/16
 *
 * @author chadj
 * @version 1.0.0
 * @since 4/19/16
 */
@Service
public class OrderDetailResourceAssembler extends EmbeddableResourceAssemblerSupport<OrderDetail, OrderDetailResource, OrderDetailsController> {

	@Autowired
	public OrderDetailResourceAssembler(EntityLinks entityLinks, RelProvider relProvider) {
		super(entityLinks, relProvider, OrderDetailsController.class, OrderDetailResource.class);
	}

	@Override
	public Link linkToSingleResource(OrderDetail entity) {
		return entityLinks.linkToSingleResource(OrderDetail.class, entity.getOrderLineNumber());
	}

	@Override
	public OrderDetailResource toResource(OrderDetail orderDetail) {
		final OrderDetailResource orderDetailResource = createResourceWithId("orderdetails/"+orderDetail.getOrderNumber(),orderDetail);
		final Link orderLink = linkTo(methodOn(OrderController.class).
				getOrder(orderDetail.getOrderNumber())).withRel("order");
		orderDetailResource.add(orderLink);
		return orderDetailResource;
	}

	@Override
	protected OrderDetailResource instantiateResource(OrderDetail entity) {
		return new OrderDetailResource(
				entity.getOrderNumber(),
				entity.getProductCode(),
				entity.getQuantityOrdered(),
				entity.getPriceEach(),
				entity.getOrderLineNumber()
		);
	}
}
