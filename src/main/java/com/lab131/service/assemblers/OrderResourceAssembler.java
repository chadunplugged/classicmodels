package com.lab131.service.assemblers;

import com.lab131.controllers.hal_json.CustomerController;
import com.lab131.controllers.hal_json.OrderController;
import com.lab131.controllers.hal_json.OrderDetailsController;
import com.lab131.entities.Order;
import com.lab131.resources.OrderResource;
import com.lab131.support.EmbeddableResourceAssemblerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RelProvider;
import org.springframework.stereotype.Service;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by chadj (dontpanic@bodybuilding.com) on 4/17/16
 *
 * @author chadj
 * @version 1.0.0
 * @since 4/17/16
 */
@Service
public class OrderResourceAssembler extends EmbeddableResourceAssemblerSupport<Order, OrderResource, OrderController> {

	@Autowired
	public OrderResourceAssembler(final EntityLinks entityLinks, final RelProvider relProvider) {
		super(entityLinks, relProvider, OrderController.class, OrderResource.class);
	}

	private OrderResource toBaseResource(Order entity) {
		final OrderResource resource = createResourceWithId(entity.getCustomerNumber(), entity);

		return resource;
	}

	@Override
	public Link linkToSingleResource(Order entity) {
		return entityLinks.linkToSingleResource(Order.class, entity.getOrderNumber());
	}

	@Override
	public OrderResource toResource(Order entity) {
		final OrderResource orderResource = createResourceWithId("order/" + entity.getOrderNumber(), entity);
		final Link customerLink = linkTo(methodOn(CustomerController.class).findOneCustomer(entity.getCustomerNumber())).withRel("customer");
		final Link orderDetailsLink = linkTo(methodOn(OrderDetailsController.class).getOrderDetails(entity.getOrderNumber())).withRel("orderdetails");
		orderResource.add(customerLink);
		orderResource.add(orderDetailsLink);
		return orderResource;
	}

	@Override
	protected OrderResource instantiateResource(Order entity) {
		return new OrderResource(
				entity.getOrderNumber(),
				entity.getOrderDate(),
				entity.getRequiredDate(),
				entity.getShippedDate(),
				entity.getStatus(),
				entity.getComments(),
				entity.getCustomerNumber()
		);
	}
}