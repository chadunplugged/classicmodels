package com.lab131.service.assemblers;

import com.lab131.controllers.hal_json.ProductController;
import com.lab131.entities.Product;
import com.lab131.resources.ProductResource;
import com.lab131.support.EmbeddableResourceAssemblerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RelProvider;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by chadj (dontpanic@bodybuilding.com) on 4/19/16
 *
 * @author chadj
 * @version 1.0.0
 * @since 4/19/16
 */
@Service
public class ProductResourceAssembler extends EmbeddableResourceAssemblerSupport<Product, ProductResource, ProductController> {

	@Autowired
	public ProductResourceAssembler(final EntityLinks entityLinks, final RelProvider relProvider) {
		super(entityLinks, relProvider, ProductController.class, ProductResource.class);
	}

	private ProductResource toBaseResource(Product entity) {
		final ProductResource resource = createResourceWithId(entity.getProductCode(), entity);

		return resource;
	}

	@Override
	public Link linkToSingleResource(Product entity) {
		return entityLinks.linkToSingleResource(Product.class, entity.getProductCode());
	}

	@Override
	public ProductResource toResource(Product entity) {
		final ProductResource ProductResource = createResourceWithId("products/" + entity.getProductCode(), entity);
		//TODO @chad there is nothing on the product entity (should there be?) that we can get back to the customer or order.
//		final Link customerLink = linkTo(methodOn(CustomerController.class).findOneCustomer(entity.())).withRel("customer");
//		ProductResource.add(customerLink);
		return ProductResource;
	}

	@Override
	protected ProductResource instantiateResource(Product entity) {
		return new ProductResource(
				entity.getProductCode(),
				entity.getProductName(),
				entity.getProductLine(),
				entity.getProductScale(),
				entity.getProductVendor(),
				entity.getProductDescription(),
				entity.getQuantityInStock(),
				entity.getBuyPrice(),
				entity.getMSRP()
		);
	}
}
