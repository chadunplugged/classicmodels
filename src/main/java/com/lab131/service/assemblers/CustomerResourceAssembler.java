package com.lab131.service.assemblers;

import com.lab131.controllers.hal_json.CustomerController;
import com.lab131.controllers.hal_json.OrderController;
import com.lab131.entities.Customer;
import com.lab131.resources.CustomerResource;
import com.lab131.support.EmbeddableResourceAssemblerSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RelProvider;
import org.springframework.stereotype.Service;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


/**
 * Created by chadj on 4/2/16.
 */
@Service
public class CustomerResourceAssembler extends EmbeddableResourceAssemblerSupport<Customer, CustomerResource, CustomerController> {

    @Autowired
    public CustomerResourceAssembler(final EntityLinks entityLinks, final RelProvider relProvider) {
        super(entityLinks, relProvider, CustomerController.class, CustomerResource.class);
    }

    private CustomerResource toBaseResource(Customer entity) {
        final CustomerResource resource =  createResourceWithId(entity.getCustomerNumber() , entity);

        return resource;
    }

    @Override
    public Link linkToSingleResource(Customer entity) {
        return entityLinks.linkToSingleResource(Customer.class, entity.getCustomerName());
    }

    @Override
    public CustomerResource toResource(Customer entity) {
        final CustomerResource customerResource = createResourceWithId(""+entity.getCustomerNumber(),entity);
	    final Link findOrdersByCustomerNumberLink = linkTo(methodOn(OrderController.class).findAllOrderNumbersByCustomerNumber(entity.getCustomerNumber())).withRel("viewCustomerOrders");
	    customerResource.add(findOrdersByCustomerNumberLink);
		return customerResource;
    }

    @Override
    protected CustomerResource instantiateResource(Customer entity) {
        return new CustomerResource(
			entity.getCustomerNumber(),
				entity.getCustomerName(),
				entity.getContactLastName(),
				entity.getContactFirstName(),
				entity.getPhone(),
				entity.getAddressLine1(),
				entity.getAddressLine2(),
				entity.getCity(),
				entity.getState(),
				entity.getPostalCode(),
				entity.getCountry(),
				entity.getSalesRepEmployeeNumber(),
				entity.getCreditLimit()
        );
    }
}
