package com.lab131.entities;

import com.lab131.utils.DataGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


/**
 * Created by chadj on 12/22/15.
 */
@Entity
@Table(name = "customers")
public class Customer {
	private static final long serialVersionUID = 298443116673285861L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int customerNumber;

	@NotNull
	private String customerName;

	private String contactLastName;

	private String contactFirstName;

	private String phone;

	private String addressLine1;

	private String addressLine2;
	private String city;

	private String state;
	private String postalCode;
	private String country;
	private String salesRepEmployeeNumber;
	private String creditLimit;

	public Customer() {
	}

	public Customer(int customerNumber, String customerName) {
		this.customerNumber = customerNumber;
		this.customerName = customerName;
	}
	
	public static Customer createNewCustomer(Customer newCustomer) {
		Customer returnCustomer = new Customer();
		returnCustomer.customerName = DataGenerator.fillStringElement(newCustomer.getCustomerName(), DataGenerator.generateCompanyName());
		returnCustomer.contactLastName = DataGenerator.fillStringElement(newCustomer.getContactLastName(), DataGenerator.generateLastName());
		returnCustomer.contactFirstName = DataGenerator.fillStringElement(newCustomer.getContactFirstName(), DataGenerator.generateFirstName());
		returnCustomer.salesRepEmployeeNumber = "1370"; //leave this, has to match a real number in the DB due to FK constraint


		returnCustomer.phone = DataGenerator.fillStringElement(newCustomer.getPhone(), DataGenerator.generatePhoneNumber());

		returnCustomer.addressLine1 = DataGenerator.fillStringElement(newCustomer.getAddressLine1(), DataGenerator.generateAddressLine());

		returnCustomer.addressLine2 = "";
		returnCustomer.city = DataGenerator.fillStringElement(newCustomer.getCity(), DataGenerator.generateCity());

		returnCustomer.state = DataGenerator.fillStringElement(newCustomer.getState(), DataGenerator.generateStateAbbreviation());
		returnCustomer.postalCode = DataGenerator.fillStringElement(newCustomer.getPostalCode(), DataGenerator.generateMulidigitNumber(0,9,5));
		returnCustomer.country = "US";
		returnCustomer.creditLimit = DataGenerator.fillStringElement(newCustomer.getCreditLimit(), DataGenerator.generateMulidigitNumber(0,9,7));
		return returnCustomer;
	}
	
	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getSalesRepEmployeeNumber() {
		return salesRepEmployeeNumber;
	}

	public void setSalesRepEmployeeNumber(String salesRepEmployeeNumber) {
		this.salesRepEmployeeNumber = salesRepEmployeeNumber;
	}

	public String getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}

	public int getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}
}
