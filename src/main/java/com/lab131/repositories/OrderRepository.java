package com.lab131.repositories;

import com.lab131.entities.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Provides the customer order data for fidAll
 */
@Repository
public interface OrderRepository extends PagingAndSortingRepository <Order, Integer> {
    Page<Order> findAllOrderNumbersByCustomerNumber(@Param("customerNumber") int customerNumber, Pageable pageable);
    List<Order> findAllOrderNumbersByCustomerNumber(@Param("customerNumber") int customerNumber);
    Page<Order> getOrderByOrderNumber(@Param("orderNumber") int orderNumber, Pageable pageable);
}