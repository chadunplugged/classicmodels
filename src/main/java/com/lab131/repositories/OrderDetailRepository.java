package com.lab131.repositories;

import com.lab131.entities.OrderDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by chadj on 12/22/15.
 */

public interface OrderDetailRepository extends PagingAndSortingRepository<OrderDetail, Integer> {
    Page<OrderDetail> findAllOrderDetailsByOrderNumber(@Param("orderNumber") int orderNumber, Pageable pageable);
	List<OrderDetail> findByOrderNumber(@Param("orderNumber") int orderNumber);
}
