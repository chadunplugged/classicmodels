package com.lab131.repositories;

import com.lab131.entities.Product;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by chadj on 3/3/16.
 */
public interface ProductRepository extends PagingAndSortingRepository<Product, Integer> {
	Product findProductByProductCode(@Param("productCode") String productCode);
}
